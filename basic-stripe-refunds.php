<?php
/*
* 
* Plugin Name: Basic Stripe Refunds
* Plugin URI: https://gitlab.com/jdvalentine/basic-stripe-refunds
* Description: Stripe refunds, dependent on MotoPress
* Version: 0.1
* Author: James Valentine
* Author URI: https://jdv.me.uk
*
*/

include('init.php'); // starts output buffering, defines utility functions

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Get the Stripe Secret API Key from MotoPress
$fullOptionName = "mphb_payment_gateway_stripe_secret_key";
$optionValue = get_option( $fullOptionName, '');
$stripe_secret_key = apply_filters( 'mphb_translate_string', $optionValue, $fullOptionName );

/**
 * Register a custom menu page.
 */
function registerRefundAdminMenu() {
    add_menu_page(
        'Stripe Refunds',
        'Stripe Refunds',
        'manage_options',
        'stripe_refunds',
        'renderPage'
    );
}
add_action( 'admin_menu', 'registerRefundAdminMenu' );


function renderPage() {

    global $stripe_secret_key;

    // Try to instantiate the API client and give an error for the benefit of users who don't realise they need MPHB 
    $warning_message = "This plugin is intended for use with the MotoPress Hotel Booking plugin and its Stripe payment gateway. Please provide the Secret Key in the MPHB configuration. ";

    if ($stripe_secret_key) {
        try {
            $stripe = new Stripe\StripeClient($stripe_secret_key);
        } catch (Exception $e) {
                show_alert($warning_message . $e->getMessage(), 'notice-error');
        }
    } else {
                show_alert($warning_message, 'notice-error');
    }


    echo '<h1>Make a Stripe refund</h1>';

    if ($_GET['msg']) { // a message in the URL, probably due to a 303 redirect
        show_alert($_GET['msg'], 'notice-info');
    }


    if (isset($_POST['processRefund'])) { // we're going to run the refund call to Stripe immediately
        $response[] = processRefund($stripe, $_POST['processRefund'], $_POST['refund_now_amount']);
    }

    if (isset($_GET['dorefund']) && isset($_GET['id'])) { // we need a second page
        echo '<h2>Refund this transaction</h2>';
        $response[] = showRefundForm($stripe, $_GET['id']);
    } else {
        echo '<h2>Recent charges</h2>';
        $response[] = listCharges($stripe);
    }

    if ($response) { // we got a message back from one of the functions
		
		foreach($response as $r) {
		if (!$r) continue;
		    show_alert($r, 'notice-info');
		}
	}

}

/**
 * Writes out a table of charges from the Stripe account, with some pagination support
 *
 */

function listCharges($stripe) {

    $options = ['limit' => 100];
    if ($_GET['ending_before']) { // pagination support
        $options['ending_before'] = $_GET['ending_before'];
    }
    if ($_GET['starting_after']) {
        $options['starting_after'] = $_GET['starting_after'];
    }

    try {
        $charges = $stripe->charges->all($options);
    } catch (exception $e) {
        return "Listing failed: {$e->getMessage()}";
eoHTML;
    }

	if (!count($charges->data)) { // Nothing in the list
		return "There are no new charges to display. You can <a href=\"/wp-admin/admin.php?page=stripe_refunds\">reload the list</a>.";
	}

    echo <<<eoHTML
    <table  class="striped table-view-list widefat">
    <thead>
        <tr>
            <th>Date</th>
            <th>Amount</th>
            <th>Name</th>
            <th>E-mail</th>
            <th>Card</th>
            <th>Last 4</th>
            <th>Refunded</th>
            <th>Action</th>
        </tr>
    </thead>
    
eoHTML;

    echo '<pre>';

	foreach($charges as $c) {
            $amount = '£' . number_format((float)$c->amount_captured/100, 2, '.', '');

            $refunded = 0;
            $refund_button = <<<eoHTML
            <a href="/wp-admin/admin.php?page=stripe_refunds&dorefund&id={$c->id}"><button>Refund</button></a>
eoHTML;
            if ($c->amount_refunded == $c->amount_captured) {
                $refunded = 'In full';
                $refund_button = '';
            } else {
                $refunded = '£' . number_format((float)$c->amount_refunded/100, 2, '.', '');
            }

            $datetime = date("Y-m-d H:i:s",$c->created);

            $tr = <<<eoHTML
            <tr>
                <td>{$datetime}</td>
                <td>{$amount}</td>
                <td>{$c->billing_details->name}</td>
                <td>{$c->billing_details->email}</td>
                <td>{$c->payment_method_details->card->brand}</td>
                <td>{$c->payment_method_details->card->last4}</td>
                <td>{$refunded}</td>
                <td>$refund_button</td>
            </tr>
eoHTML;
            //print_r($c);
            echo $tr ;

	}	
    echo '</table>';




    echo <<<eoHTML
        <br />
        <div style="text-align: right">
eoHTML;
	if ($charges->data[0]['id']) {
		echo <<<eoHTML
            <a href="/wp-admin/admin.php?page=stripe_refunds&ending_before={$charges->data[0]['id']}"><button>&lt; Newer</button></a>
eoHTML;
	}
    if (!$charges->data[0]['id'] || $charges->has_more) { // we may have some more - it's likely actually! Poor man's pagination.
		echo <<<eoHTML
            <a href="/wp-admin/admin.php?page=stripe_refunds&starting_after={$c->id}"><button>Older &gt;</button></a>
eoHTML;
    }
    echo <<<eoHTML
        </div>
eoHTML;

}

/**
 * Shows the details of a charge, along with a form to actually make a refund
 *
 */

function showRefundForm($stripe, $id) {

	$c = $stripe->charges->retrieve($id);

    $amount = '£' . number_format((float)$c->amount_captured/100, 2, '.', '');

    $refunded = 0;

    if ($c->amount_refunded == $c->amount_captured) {
        $refunded = 'In full';
        $refund_button = '';
    } else {
        $refunded = '£' . number_format((float)$c->amount_refunded/100, 2, '.', '');
    }

    $refunds_processed = count($c->refunds);

    $datetime = date("Y-m-d H:i:s",$c->created);

    echo <<<eoHTML


    <table  class="striped table-view-list widefat">
    <tr>
        <td>Date</td>
        <td>{$datetime}</td>
    </tr>
    <tr>
        <td>Amount</td>
        <td>{$amount}</td>
    </tr>
    <tr>
        <td>Name</td>
        <td>{$c->billing_details->name}</td>
    </tr>
    <tr>
        <td>E-mail</td>
        <td>{$c->billing_details->email}</td>
    </tr>
    <tr>
        <td>Card</td>
        <td>{$c->payment_method_details->card->brand}</td>
    </tr>
    <tr>
        <td>Last 4</td>
        <td>{$c->payment_method_details->card->last4}</td>
    </tr>
    <tr>
        <td>Refunds processed</td>
        <td>{$refunds_processed}</td>
    </tr>
    <tr>
        <td>Refunded so far</td>
        <td>{$refunded}</td>
    </tr>
    <tr>
    <form action="/wp-admin/admin.php?page=stripe_refunds&dorefund&id={$c->id}" method="post">
    <td>Amount to refund now:</td>
    <td>£<input type="text" name="refund_now_amount" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><button name="processRefund" value="{$c->id}" type="submit">Refund now</button></td>
    </form>
    </tr>
eoHTML;


}

/**
 * Run on POST to actually request the refund with the Stripe API and return any exception message
 * implements Post/Redirect/Get to avoid the same refund being submitted more than once.
 *
 */

function processRefund($stripe, $id, $amount) {

    $amount_to_refund = (float)$amount;
    $amount_in_pence = $amount_to_refund * 100;

    try {
        $result = $stripe->refunds->create([
              'charge' => $id,
              'amount' => $amount_in_pence,
        ]);

        sleep(1);

    } catch (exception $e) {
        return "Refund failed: {$e->getMessage()}";
    }

    $msg = "A refund of £{$amount_to_refund} has been requested from Stripe for this charge and should be included in <em>Refunded so far</em> below";
    $encoded_msg = urlencode($msg);
    $location = get_bloginfo('wpurl') . "/wp-admin/admin.php?page=stripe_refunds&dorefund&id=$id&msg=$encoded_msg";
    wp_safe_redirect( $location, 303 );
    exit;
}


// vim: ts=4 sw=4 expandtab smartindent shiftwidth=4 smarttab
