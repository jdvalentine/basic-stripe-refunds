﻿=== Basic Stripe Refunds for Motopress ===
Contributors: James Valentine, Kohera Limited
Donate link: https://kohera.co.uk
Tags: Stripe, payments, refunds, MotoPress
Requires at least: 5.4
Tested up to: 5.8
Requires PHP: 5.6
Stable tag: 1.7.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Basic refund facility against Stripe charges. Intended for use with MotoPress Hotel Bookings (where it gets its config from)

== Description ==

Are you wanting to do ad hoc, largely unrestricted refunds against your Stripe account customer charges? Thought so!
I wrote this plugin to allow me to refund security deposit payments made outside of the MPHB plugin, but for my own convenience, this plugin
is dependent on the Stripe configuration in that plugin (and in fact, the version of the Stripe API which is distributed
with it). I've done a day's hacking on this - it's far from perfect but might be a reasonable starting point for you.

Use it with caution! You could refund payments to customers and there'll be no audit trail of that against the booking in MPHB.

You have been warned.

[Find the source here](https://gitlab.com/jdvalentine/basic-stripe-refunds)
Find an installable Zip here](https://gitlab.com/jdvalentine/basic-stripe-refunds/-/blob/main/dist/)

