<?php
ob_start();

       // Autoloading the Stripe API distributed with MotoPress Hotel Bookings (as this plugin is intended for use with it)
        spl_autoload_register(function ($className) {
            // "Stripe\Checkout\Session"
            $className = ltrim($className, '\\');

            if (strpos($className, 'Stripe') !== 0) {
                return false;
            }

            // "lib\Checkout\Session"
            $pluginFile = str_replace('Stripe\\', 'lib\\', $className);
            // "lib/Checkout/Session"
            $pluginFile = str_replace('\\', DIRECTORY_SEPARATOR, $pluginFile);
            // "lib/Checkout/Session.php"
            $pluginFile .= '.php';
            // ".../vendors/stripe-api/lib/Checkout/Session.php"
            $pluginFile = WP_PLUGIN_DIR . '/motopress-hotel-booking/vendors/stripe-api/' . $pluginFile;

            if (file_exists($pluginFile)) {
                require $pluginFile;
                return true;
            } else {
                return false;
            }
        });

function show_alert($msg, $class='notice-info') { // for use everywhere we need to alert the user
    echo <<<eoHTML
        <div class="notice {$class}">
            <p>$msg</p>
        </div>
eoHTML;
}


function check_required_plugins() { // We need to make it clear in the Admin when the plugin we depend on is not installed
    if ( current_user_can( 'activate_plugins' ) ) {
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        if ( ! is_plugin_active( 'motopress-hotel-booking/motopress-hotel-booking.php' ) )
            add_action( 'admin_notices', 'mphb_required' );
    }
}
add_action( 'plugins_loaded', 'check_required_plugins' );


function mphb_required() { 
    $url = 'https://motopress.com/products/hotel-booking/';
    echo '
    <div class="error">
        <p>For the Stripe Refunds plugin to work, the <a href="' . $url . '">MotoPress Hotel Booking</a> plugin is required.</p>
    </div>
    ';
}
